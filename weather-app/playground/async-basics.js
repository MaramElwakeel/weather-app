console.log('Starting app');

//we aren't telling node to wait 2 secs, we are actually registering a callback that's gonna get fired in 2 secs.
//which means that node can do other things while those 2 secs. are happining.
setTimeout(() => {
  console.log('Inside of callback');
}, 2000);

setTimeout(() => {
  console.log(' third');
}, 100);
setTimeout(() => {
  console.log('Second timeout');
}, 0);

console.log('Finishing up');
setTimeout(() => {
  console.log('khhcallback');
}, 2000);
