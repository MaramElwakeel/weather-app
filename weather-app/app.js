//Request is designed to be the simplest way possible to make http calls.
//It supports HTTPS and follows redirects by default.

const yargs = require('yargs');
const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

const argv = yargs.options({
  a: {
    demand: true,
    alias: 'address',
    describe: 'Address to fetch weather for.',
    string: true
  }
}).help().alias('help', 'h').argv;

geocode.geocodeAddress(argv.address, (errorMessage, results) => {
  if(errorMessage) {
    console.log(errorMessage);
  }
  else {
    console.log(JSON.stringify(results.address));
    weather.getWeather(results.latitude, results.longitude, (errorMessage, weatherResults) => {
      if(errorMessage) {
        console.log(errorMessage);
      }
      else {
        console.log(`It's currently ${weatherResults.temperature}. \nIt feels like ${weatherResults.apparentTemperature}.`);
      }
    });
  }
});
